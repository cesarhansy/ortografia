﻿using UnityEngine;
using System.Collections;

public class Player {

	private int puntaje;
	private string nombre;

	public Player (string newNombre){
		this.puntaje = 0;
		this.nombre = newNombre;
	}

	public void setPuntaje(int newPuntaje){
		this.puntaje += newPuntaje;
	}
}
