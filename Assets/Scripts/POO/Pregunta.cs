﻿using UnityEngine;
using System.Collections;

public class Pregunta {

	private string frase;
	private string incorrecta;
	private string correcta;
	private string incorrecta1;
	private string incorrecta2;

	public Pregunta(string newFrase, string newCorrecta, string newIncorrecta1, string newIncorrecta2){
		this.frase = newFrase;
		this.correcta = newCorrecta;
		this.incorrecta1 = newIncorrecta1;
		this.incorrecta2 = newIncorrecta2;
	}

	public string getFrase(){
		return frase;
	}

	public string getCorrecta(){
		return correcta;
	}

	public string getIncorrecta1(){
		return incorrecta1;
	}

	public string getIncorrecta2(){
		return incorrecta2;
	}
}
