﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {

	private int typePaper = 0;
	// Use this for initialization
	void Start () {
		float probability = Random.Range (0f, 1f);
		selectTypePaper(probability);
		putOptions();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void selectTypePaper(float probability){
		if(probability >= 0 && probability < 0.2f){
			//Seleccionar Papel 1
			typePaper = 1;
		}
		else if(probability >= 0.2f && probability < 0.4f){
			//Seleccionar Papel 2
			typePaper = 2;
		}
		else if(probability >= 0.4f && probability < 0.6f){
			//Seleccionar Papel 3
			typePaper = 3;
		}
		else if(probability >= 0.6f && probability < 0.8f){
			//Seleccionar Papel 4
			typePaper = 4;
		}
		else{
			//Seleccionar Papel 5
			typePaper = 5;
		}
	}

	void putOptions(){
	}
}
